<script>
(function(){ 
  	if ({{emailaddress_from_form}}!= 'undefined' && "{{referral_campaign_id_from_cookie}} != 'undefined' && {{share_token_from_cookie}} != 'undefined') {
      	var url = 'https://hooks.zapier.com/{{zapier_url_provided}}}'
    	var xhr = new XMLHttpRequest();
    	xhr.open("POST", url, true);
    	//xhr.setRequestHeader("Content-type", "application/json");
    
      	var data = JSON.stringify({
            "referral": {
                "X_ORG_ID": "{{X_ORG_ID_PROVIDED}}",
              	"Auth_token": "{{AUTH TOKEN PROVIDED}}",
              	"infl_referral_campaign_id":"{{referral_campaign_id_from_cookie}}", 
                "status": "won", 
                "prospect": { 
                    "email": "{{emailaddress_from_form}}", 
                },
                "contact": {
                  "infl_share_token": "{{share_token_from_cookie}}" 
                }
            }
        });
      
    	xhr.onreadystatechange = function () {
        	console.log(xhr.status);
          	if (xhr.status === 200) {
        		var json = JSON.parse(xhr.responseText);
              	console.log(json)
        	}
    	}
        xhr.send(data);
    }
})();
</script>